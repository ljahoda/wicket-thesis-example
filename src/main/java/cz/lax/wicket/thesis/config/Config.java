package cz.lax.wicket.thesis.config;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 11.03.14
 * Email: lukas@lukasjahoda.cz
 */
public abstract class Config {
	private static final Boolean DEVELOPMENT_MODE = Boolean.TRUE;

	/**
	 * Instantiates a new Config.
	 */
	private Config() {
		throw new IllegalAccessError();
	}

	/**
	 * Is devel.
	 *
	 * @return the boolean
	 */
	public static boolean isDevel() {
		return DEVELOPMENT_MODE; // this should be in some property file
	}
}
