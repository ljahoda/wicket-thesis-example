package cz.lax.wicket.thesis.pages;

import org.apache.wicket.markup.html.WebPage;

/**
 * User: ljahoda
 * Name: Lukas Jahoda
 * Date: 08.11.13
 * Email: lukas@lukasjahoda.cz
 */
public abstract class AbstractPage extends WebPage {

	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7900446983852324729L;
}
