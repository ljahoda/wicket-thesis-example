package cz.lax.wicket.thesis.pages;

import cz.lax.wicket.thesis.model.registration.*;
import cz.lax.wicket.thesis.panels.registration.*;
import org.apache.wicket.model.*;

/**
 * User: ljahoda
 * Name: Lukas Jahoda
 * Date: 07.11.13
 * Email: lukas@lukasjahoda.cz
 */
public class HomePage extends AbstractPage {
	private static final long serialVersionUID = 1L;

	public static final String REGISTRATION_PANEL_ID = "registrationPanel";

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new RegistrationPanel(REGISTRATION_PANEL_ID, new CompoundPropertyModel<>(new Registration())));
	}
}
