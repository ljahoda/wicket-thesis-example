package cz.lax.wicket.thesis.panels.registration;

import com.googlecode.wicket.jquery.core.*;
import com.googlecode.wicket.jquery.ui.widget.dialog.*;
import cz.lax.wicket.thesis.domain.*;
import cz.lax.wicket.thesis.model.registration.*;
import org.apache.wicket.ajax.*;
import org.apache.wicket.datetime.markup.html.basic.*;
import org.apache.wicket.markup.html.*;
import org.apache.wicket.markup.html.basic.*;
import org.apache.wicket.model.*;

import java.util.*;

import static org.apache.wicket.model.PropertyModel.*;
import static cz.lax.wicket.thesis.model.registration.Registration.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 14.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class DialogPanel extends AbstractDialog<Registration> {
	private static final long serialVersionUID = 1L;

	private final static String DATE_PATTERN = "dd.MM.yyyy";
	private final static String CONTAINER_ID = "dialogContainer";

	private WebMarkupContainer contentContainer;

	/**
	 * Instantiates a new Response dialog panel.
	 *
	 * @param id the id
	 * @param title the title
	 * @param model the model
	 * @param modal the modal
	 */
	public DialogPanel(String id, IModel<String> title, IModel<Registration> model, boolean modal) {
		super(id, title, model, modal);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		contentContainer = new WebMarkupContainer(CONTAINER_ID);
		contentContainer.setOutputMarkupPlaceholderTag(true);

		contentContainer.add(new Label(PROPERTY_FIRST_NAME, of(getModel(), PROPERTY_FIRST_NAME)));
		contentContainer.add(new Label(PROPERTY_LAST_NAME, of(getModel(), PROPERTY_LAST_NAME)));
		contentContainer.add(new EnumLabel<>(PROPERTY_GENDER, new PropertyModel<Gender>(getModel(), PROPERTY_GENDER)));
		contentContainer.add(DateLabel.forDatePattern(PROPERTY_DATE_OF_BIRTH, new PropertyModel<Date>(getModel(), PROPERTY_DATE_OF_BIRTH), DATE_PATTERN));
		contentContainer.add(new Label(PROPERTY_EMAIL, of(getModel(), PROPERTY_EMAIL)));
		contentContainer.add(new EnumLabel<>(PROPERTY_SOURCE, new PropertyModel<Source>(getModel(), PROPERTY_SOURCE)));
		add(contentContainer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onOpen(AjaxRequestTarget target) {
		target.add(this.contentContainer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onConfigure(JQueryBehavior behavior) {
		super.onConfigure(behavior);

		behavior.setOption("show", new Options().set("effect", Options.asString("fadeIn")).set("duration", 1000));
		behavior.setOption("hide", new Options().set("effect", Options.asString("explode")).set("duration", 1000));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClose(AjaxRequestTarget target, DialogButton button) {

	}
}
