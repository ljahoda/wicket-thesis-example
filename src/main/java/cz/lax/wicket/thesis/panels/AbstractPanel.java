package cz.lax.wicket.thesis.panels;

import org.apache.wicket.*;
import org.apache.wicket.markup.html.panel.*;
import org.apache.wicket.model.*;

/**
 * This is an abstract generic panel, which contains all the common features
 * for all the panels. It also implements IGenereciComponent to add generic
 * types to model objects.
 *
 * User: ljahoda
 * Name: Lukas Jahoda
 * Date: 08.11.13
 * Email: lukas@lukasjahoda.cz
 */
public class AbstractPanel<T> extends Panel implements IGenericComponent<T> {

	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new Abstract panel.
	 *
	 * @param id the id
	 */
	public AbstractPanel(final String id) {
		this(id, null);
	}

	/**
	 * Instantiates a new Abstract panel.
	 *
	 * @param id the id
	 * @param model the model
	 */
	public AbstractPanel(final String id, final IModel<T> model) {
		super(id, model);
	}

	/**
	 * On initialize.
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();
	}

	/**
	 * Gets model.
	 *
	 * @return the model
	 */
	@SuppressWarnings("unchecked")
	@Override
	public IModel<T> getModel() {
		return (IModel<T>) getDefaultModel();
	}

	/**
	 * Sets model.
	 *
	 * @param model the model
	 */
	@Override
	public void setModel(final IModel<T> model) {
		setDefaultModel(model);
	}

	/**
	 * Gets model object.
	 *
	 * @return the model object
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T getModelObject() {
		return (T) getDefaultModelObject();
	}

	/**
	 * Sets model object.
	 *
	 * @param object the object
	 */
	@Override
	public void setModelObject(final T object) {
		setDefaultModelObject(object);
	}
}
