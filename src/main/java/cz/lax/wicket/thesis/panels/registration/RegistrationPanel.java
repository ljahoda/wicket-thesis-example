package cz.lax.wicket.thesis.panels.registration;

import com.googlecode.wicket.jquery.core.*;
import com.googlecode.wicket.jquery.ui.form.button.*;
import cz.lax.wicket.thesis.behavior.*;
import cz.lax.wicket.thesis.behavior.jquery.*;
import cz.lax.wicket.thesis.component.*;
import cz.lax.wicket.thesis.domain.*;
import cz.lax.wicket.thesis.model.registration.*;
import cz.lax.wicket.thesis.panels.*;
import org.apache.wicket.ajax.*;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.form.validation.*;
import org.apache.wicket.model.*;
import org.apache.wicket.validation.validator.*;

import java.util.*;

import static cz.lax.wicket.thesis.model.registration.Registration.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class RegistrationPanel extends AbstractCompoundPanel<Registration> {
	private static final long serialVersionUID = 1L;

	public static final String FORM_ID = "registrationForm";
	public static final String FIRST_NAME_ROW_ID = "firstNameRow";
	public static final String LAST_NAME_ROW_ID = "lastNameRow";
	public static final String GENDER_ROW_ID = "genderRow";
	public static final String DATE_OF_BIRTH_ROW_ID = "dateOfBirthRow";
	public static final String EMAIL_ROW_ID = "emailRow";
	public static final String PASSWORD_ROW_ID = "passwordRow";
	public static final String CONFIRM_PASSWORD_ROW_ID = "confirmPasswordRow";
	public static final String SOURCE_ROW_ID = "sourceRow";
	public static final String AGREEMENT_ROW_ID = "agreementRow";
	public static final String SUBMIT_ID = "submitRegistration";
	public static final String RESPONSE_PANEL_ID = "responsePanel";

	private DialogPanel responsePanel;

	/**
	 * Instantiates a new Registration panel.
	 *
	 * @param id the id
	 * @param model the model
	 */
	public RegistrationPanel(String id, CompoundPropertyModel<Registration> model) {
		super(id, model);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		Form<Registration> form = new Form<>(FORM_ID, getModel());
		form.setMarkupId(FORM_ID);

		// first name field
		final FormRowPanel firstNameRow = new FormRowPanel(FIRST_NAME_ROW_ID);
		RequiredTextField<String> firstNameField = new RequiredTextField<>(PROPERTY_FIRST_NAME);
		firstNameField.add(new StringValidator(1, 50));
		firstNameField.add(new OnChangeAjaxFieldBehavior(firstNameRow));
		firstNameRow.add(firstNameField);
		form.add(firstNameRow);

		// last name field
		final FormRowPanel lastNameRow = new FormRowPanel(LAST_NAME_ROW_ID);
		RequiredTextField<String> lastNameField = new RequiredTextField<>(PROPERTY_LAST_NAME);
		lastNameField.add(new StringValidator(1, 50));
		lastNameField.add(new OnChangeAjaxFieldBehavior(lastNameRow));
		lastNameRow.add(lastNameField);
		form.add(lastNameRow);

		// gender radio
		final FormRowPanel genderRow = new FormRowPanel(GENDER_ROW_ID);
		PrettyRadioChoice<Gender> genderRadioChoice = new PrettyRadioChoice<>(PROPERTY_GENDER, Model.ofList(Arrays.asList(Gender.values())));
		genderRadioChoice.setRequired(true);
		genderRow.add(genderRadioChoice);
		form.add(genderRow);

		// date of birth
		final FormRowPanel dateOfBirthRow = new FormRowPanel(DATE_OF_BIRTH_ROW_ID);
		CustomDatepicker dateOfBirthField = new CustomDatepicker(PROPERTY_DATE_OF_BIRTH);
		dateOfBirthField.setRequired(Boolean.TRUE);
		dateOfBirthField.add(new OnChangeAjaxFieldBehavior(dateOfBirthRow));
		dateOfBirthRow.add(dateOfBirthField);
		form.add(dateOfBirthRow);

		// email field
		final FormRowPanel emailRow = new FormRowPanel(EMAIL_ROW_ID);
		EmailTextField emailField = new EmailTextField(PROPERTY_EMAIL);
		emailField.setRequired(true);
		emailField.add(new OnChangeAjaxFieldBehavior(emailRow));
		emailRow.add(emailField);
		form.add(emailRow);

		// password text field
		final FormRowPanel passwordRow = new FormRowPanel(PASSWORD_ROW_ID);
		PasswordTextField passwordTextField = new PasswordTextField(PROPERTY_PASSWORD);
		passwordTextField.setRequired(true);
		passwordTextField.add(new OnChangeAjaxFieldBehavior(passwordRow));
		passwordRow.add(passwordTextField);
		form.add(passwordRow);

		// confirm password text field
		final FormRowPanel confirmPasswordRow = new FormRowPanel(CONFIRM_PASSWORD_ROW_ID);
		PasswordTextField confirmPasswordField = new PasswordTextField(PROPERTY_CONFIRM_PASSWORD);
		confirmPasswordField.setRequired(true);
		confirmPasswordField.add(new OnChangeAjaxFieldBehavior(confirmPasswordRow));
		confirmPasswordRow.add(confirmPasswordField);
		form.add(confirmPasswordRow);

		// password equal validator
		form.add(new EqualPasswordInputValidator(passwordTextField, confirmPasswordField));

		// source select field
		final FormRowPanel sourceRow = new FormRowPanel(SOURCE_ROW_ID);
		DropDownChoice<Source> sourceSelectField = new DropDownChoice<Source>(PROPERTY_SOURCE, Model.ofList(Arrays.asList(Source.values())), new EnumChoiceRenderer<Source>()) {
			private static final long serialVersionUID = 1L;

			/**
			 * {@inheritDoc}
			 */
			@Override
			protected String getNullKeyDisplayValue() {
				return getString("registration.source");
			}
		};
		sourceSelectField.setRequired(true);
		sourceSelectField.add(new SelectBoxItBehavior(IJQueryWidget.JQueryWidget.getSelector(sourceSelectField)));
		sourceSelectField.add(new OnChangeAjaxFieldBehavior(sourceRow));
		sourceRow.add(sourceSelectField);
		form.add(sourceRow);

		// agreement checkbox
		final FormRowPanel agreementRow = new FormRowPanel(AGREEMENT_ROW_ID);
		CheckBox agreementCheckbox = new CheckBox(PROPERTY_AGREEMENT);
		agreementCheckbox.setRequired(true);
		agreementCheckbox.add(new PrettyCheckableBehavior(IJQueryWidget.JQueryWidget.getSelector(agreementCheckbox)));
		agreementCheckbox.add(new OnChangeAjaxFieldBehavior(agreementRow));
		agreementRow.add(agreementCheckbox);
		form.add(agreementRow);

		responsePanel = new DialogPanel(RESPONSE_PANEL_ID, new ResourceModel("response.dialog.title"), Model.of(new Registration()), true);
		responsePanel.setMarkupId(RESPONSE_PANEL_ID);
		add(responsePanel);

		// submit button
		AjaxButton submitButton = new AjaxButton(SUBMIT_ID, form) {
			private static final long serialVersionUID = 1L;

			/**
			 * {@inheritDoc}
			 */
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				super.onSubmit(target, form);

				Registration registration = (Registration) form.getModelObject();

				// update response panel
				responsePanel.setModelObject(registration);
				responsePanel.open(target);

				// clear the form
				form.setDefaultModel(new CompoundPropertyModel<>(new Registration()));
				target.add(form);
			}

			/**
			 * {@inheritDoc}
			 */
			@Override
			protected void onError(AjaxRequestTarget target, Form<?> form) {
				firstNameRow.onError(target);
				lastNameRow.onError(target);
				genderRow.onError(target);
				dateOfBirthRow.onError(target);
				emailRow.onError(target);
				passwordRow.onError(target);
				confirmPasswordRow.onError(target);
				sourceRow.onError(target);
				agreementRow.onError(target);
			}
		};
		submitButton.setMarkupId(SUBMIT_ID);
		form.add(submitButton);

		add(form);
	}
}
