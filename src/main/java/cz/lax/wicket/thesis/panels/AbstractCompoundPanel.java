package cz.lax.wicket.thesis.panels;

import org.apache.wicket.model.*;

/**
 * Abstract compound panel extends common abstract panel, but it only accept
 * Compound Property Model in the constructor.
 *
 * User: ljahoda
 * Name: Lukas Jahoda
 * Date: 08.11.13
 * Email: lukas@lukasjahoda.cz
 */
public abstract class AbstractCompoundPanel<T> extends AbstractPanel<T> {
	/**
	 * The constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new Abstract panel.
	 *
	 * @param id the id
	 */
	public AbstractCompoundPanel(final String id) {
		this(id, null);
	}

	/**
	 * Instantiates a new Abstract panel.
	 *
	 * @param id the id
	 * @param model the model
	 */
	public AbstractCompoundPanel(final String id, final CompoundPropertyModel<T> model) {
		super(id, model);
	}

	/**
	 * Gets model.
	 *
	 * @return the model
	 */
	@SuppressWarnings("unchecked")
	@Override
	public CompoundPropertyModel<T> getModel() {
		return (CompoundPropertyModel<T>) getDefaultModel();
	}
}
