package cz.lax.wicket.thesis.panels;

import org.apache.wicket.markup.html.panel.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 14.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class InvisiblePanel extends Panel {
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new Invisible panel.
	 *
	 * @param id the id
	 */
	public InvisiblePanel(String id) {
		super(id);

		this.setMarkupId(id);
		this.setOutputMarkupPlaceholderTag(true);
		this.setVisible(false);
	}
}
