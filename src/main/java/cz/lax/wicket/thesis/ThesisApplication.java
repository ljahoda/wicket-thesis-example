package cz.lax.wicket.thesis;

import cz.lax.wicket.thesis.config.*;
import cz.lax.wicket.thesis.pages.*;
import org.apache.wicket.*;
import org.apache.wicket.protocol.http.*;

/**
 * User: ljahoda
 * Name: Lukas Jahoda
 * Date: 07.11.13
 * Email: lukas@lukasjahoda.cz
 */
public class ThesisApplication extends WebApplication {

	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init() {
		// markup settings
		getMarkupSettings().setStripComments(false);
		getMarkupSettings().setCompressWhitespace(false);
		getMarkupSettings().setStripWicketTags(true);

		// sources configuration
		getResourceSettings().setUseDefaultOnMissingResource(true);
		getResourceSettings().setThrowExceptionOnMissingResource(false);

		// debug ajax window
		getDebugSettings().setAjaxDebugModeEnabled(true);
		getDebugSettings().setDevelopmentUtilitiesEnabled(true);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RuntimeConfigurationType getConfigurationType() {
		return Config.isDevel() ? RuntimeConfigurationType.DEVELOPMENT : RuntimeConfigurationType.DEPLOYMENT;
	}
}
