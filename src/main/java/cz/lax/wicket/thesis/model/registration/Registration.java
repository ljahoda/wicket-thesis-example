package cz.lax.wicket.thesis.model.registration;

import cz.lax.wicket.thesis.domain.*;
import cz.lax.wicket.thesis.model.*;

import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class Registration extends AbstractModel {

	public static final String PROPERTY_FIRST_NAME = "firstName";
	public static final String PROPERTY_LAST_NAME = "lastName";
	public static final String PROPERTY_GENDER = "gender";
	public static final String PROPERTY_DATE_OF_BIRTH = "dateOfBirth";
	public static final String PROPERTY_EMAIL = "email";
	public static final String PROPERTY_PASSWORD = "password";
	public static final String PROPERTY_CONFIRM_PASSWORD = "confirmPassword";
	public static final String PROPERTY_SOURCE = "source";
	public static final String PROPERTY_AGREEMENT = "agreement";

	private static final long serialVersionUID = 1L;

	String firstName;
	String lastName;
	Gender gender;
	Date dateOfBirth;
	String email;
	String password;
	String confirmPassword;
	Source source;
	Boolean agreement;

	/**
	 * Gets first name.
	 *
	 * @return the first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets first name.
	 *
	 * @param firstName the first name
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets full name.
	 *
	 * @return the full name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets full name.
	 *
	 * @param lastName the full name
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets gender.
	 *
	 * @return the gender
	 */
	public Gender getGender() {
		return gender;
	}

	/**
	 * Sets gender.
	 *
	 * @param gender the gender
	 */
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	/**
	 * Gets date of birth.
	 *
	 * @return the date of birth
	 */
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * Sets date of birth.
	 *
	 * @param dateOfBirth the date of birth
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Gets email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets email.
	 *
	 * @param email the email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets password.
	 *
	 * @param password the password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets confirm password.
	 *
	 * @return the confirm password
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * Sets confirm password.
	 *
	 * @param confirmPassword the confirm password
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	/**
	 * Gets source.
	 *
	 * @return the source
	 */
	public Source getSource() {
		return source;
	}

	/**
	 * Sets source.
	 *
	 * @param source the source
	 */
	public void setSource(Source source) {
		this.source = source;
	}

	/**
	 * Gets agreement.
	 *
	 * @return the agreement
	 */
	public Boolean getAgreement() {
		return agreement;
	}

	/**
	 * Sets agreement.
	 *
	 * @param agreement the agreement
	 */
	public void setAgreement(Boolean agreement) {
		this.agreement = agreement;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return "Registration{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", gender=" + gender +
				", dateOfBirth=" + dateOfBirth +
				", email='" + email + '\'' +
				", password='" + password + '\'' +
				", confirmPassword='" + confirmPassword + '\'' +
				", source=" + source +
				", agreement=" + agreement +
				'}';
	}
}
