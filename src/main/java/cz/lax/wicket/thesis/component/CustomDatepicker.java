package cz.lax.wicket.thesis.component;

import com.googlecode.wicket.jquery.core.*;
import com.googlecode.wicket.jquery.ui.form.datepicker.*;
import org.apache.wicket.markup.head.*;
import org.apache.wicket.request.resource.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class CustomDatepicker extends DatePicker {
	private static final long serialVersionUID = 1L;

	private static final String DATE_PATTERN = "dd.mm.yy";
	private static final String DEFAULT_LANGUAGE = "cs";

	private static final Options DEFAULT_OPTIONS = new Options()
			.set("changeYear", true)
			.set("changeMonth", true)
			.set("yearRange", Options.asString("-100:+0"));

	/**
	 * Instantiates a new Custom datepicker.
	 *
	 * @param id the id
	 */
	public CustomDatepicker(String id) {
		super(id, DATE_PATTERN, DEFAULT_OPTIONS);
	}

	/**
	 * Instantiates a new Custom datepicker.
	 *
	 * @param id the id
	 * @param pattern the pattern
	 * @param options the options
	 */
	public CustomDatepicker(String id, String pattern, Options options) {
		super(id, pattern, options);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void renderHead(final IHeaderResponse response) {
		super.renderHead(response);

		response.render(new PriorityHeaderItem(JavaScriptHeaderItem.forReference(new JavaScriptResourceReference(CustomDatepicker.class, "jquery-ui-i18n.min.js"))));
		response.render(OnDomReadyHeaderItem.forScript("$.datepicker.setDefaults($.datepicker.regional['" + DEFAULT_LANGUAGE + "']);"));
	}
}
