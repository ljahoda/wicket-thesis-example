package cz.lax.wicket.thesis.component;

import com.googlecode.wicket.jquery.core.*;
import cz.lax.wicket.thesis.behavior.jquery.*;
import org.apache.commons.lang3.*;
import org.apache.wicket.ajax.*;
import org.apache.wicket.feedback.*;
import org.apache.wicket.markup.html.basic.*;
import org.apache.wicket.markup.html.border.*;
import org.apache.wicket.model.*;

import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class FormRowPanel extends Border implements IFeedback, IJQueryWidget {
	private static final long serialVersionUID = 1L;

	private static final String ERROR_ID = "errorMessage";

	private Label errorLabel;
	private transient String mySelector;

	/**
	 * Instantiates a new Form row panel.
	 *
	 * @param id the id
	 */
	public FormRowPanel(String id) {
		super(id);

		setOutputMarkupId(true);
		errorLabel = new Label(ERROR_ID, new LabelMessageModel());
		addToBorder(errorLabel.setOutputMarkupId(true));
		add(newWidgetBehavior(getMySelector()));
	}

	/**
	 * Gets my selector.
	 *
	 * @return the my selector
	 */
	public String getMySelector() {
		if (mySelector == null) {
			mySelector = JQueryWidget.getSelector(this);
		}
		return mySelector;
	}

	/**
	 * Gets messages filter.
	 *
	 * @return Let subclass specify some other filter
	 */
	protected IFeedbackMessageFilter getMessagesFilter() {
		return new ContainerFeedbackMessageFilter(this);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onBeforeRender(JQueryBehavior behavior) {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onConfigure(JQueryBehavior behavior) {
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public JQueryBehavior newWidgetBehavior(String selector) {
		return new JQueryBehavior(selector, "formRowInit");
	}

	/**
	 * On error.
	 *
	 * @param target the target
	 */
	public void onError(AjaxRequestTarget target) {
		boolean isErr = StringUtils.isNotBlank(errorLabel.getDefaultModelObjectAsString());
		if (isVisible()) {
			errorLabel.add(new TemporaryJQueryBehavior(getMySelector(), "formRowOkError", new Options("err", isErr).set("ok", !isErr)));
			target.add(errorLabel);
		}
	}

	/**
	 * On success.
	 *
	 * @param target the target
	 */
	public void onSuccess(AjaxRequestTarget target) {
		if (isVisible()) {
			errorLabel.add(new TemporaryJQueryBehavior(getMySelector(), "formRowOkError", new Options("ok",Boolean.TRUE)));
			target.add(errorLabel);
		}
	}

	/**
	 * The type Label message model.
	 */
	private class LabelMessageModel extends AbstractReadOnlyModel<String> {
		private static final long serialVersionUID = 1L;

		@Override
		public String getObject() {
			List<FeedbackMessage> messages = new FeedbackCollector(getPage()).collect(getMessagesFilter());
			return messages.size() == 0 ? "" : messages.get(0).getMessage().toString();
		}

	}
}
