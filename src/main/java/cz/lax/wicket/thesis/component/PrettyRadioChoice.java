package cz.lax.wicket.thesis.component;

import com.googlecode.wicket.jquery.core.*;
import cz.lax.wicket.thesis.behavior.jquery.*;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.model.*;
import org.apache.wicket.util.lang.*;
import org.apache.wicket.util.value.*;

import java.util.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 * @param <T>  the type parameter
 */
public class PrettyRadioChoice<T extends Enum<T>> extends RadioChoice<T> {
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new Pretty radio choice.
	 *
	 * @param id the id
	 * @param choices the choices
	 */
	public PrettyRadioChoice(String id, IModel<? extends List<? extends T>> choices) {
		super(id, choices, new EnumChoiceRenderer<T>());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onInitialize() {
		super.onInitialize();

		add(new PrettyCheckableBehavior("#" + getMarkupId(true) + " input[type=\"radio\"]"));

		JQueryBehavior hideLabelBehavior = new JQueryBehavior(getMarkupId(true)) {
			private static final long serialVersionUID = 1L;

			/**
			 * {@inheritDoc}
			 */
			@Override
			protected String $() {
				return String.format("jQuery(function() { jQuery('#%s > label').remove(); });", selector);
			}
		};

		add(hideLabelBehavior);
		setSuffix("");
	}

	/**
	 * {@inheritDoc}
	 */
	protected IValueMap getAdditionalAttributes(final int index, final T choice) {
		ValueMap valueMap = new ValueMap();
		valueMap.put("class", "pretty");
		valueMap.put("data-label", getString(Classes.simpleName(choice.getDeclaringClass()) + '.' + choice.name()));
		return valueMap;
	}
}
