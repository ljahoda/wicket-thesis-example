package cz.lax.wicket.thesis.behavior.jquery;

import com.googlecode.wicket.jquery.core.*;
import org.apache.wicket.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class TemporaryJQueryBehavior extends JQueryBehavior {
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new Temporary j query behavior.
	 *
	 * @param selector the selector
	 * @param method the method
	 * @param options the options
	 */
	public TemporaryJQueryBehavior(String selector, String method, Options options) {
		super(selector, method, options);
	}

	/**
	 * Instantiates a new Temporary j query behavior.
	 *
	 * @param selector the selector
	 * @param method the method
	 */
	public TemporaryJQueryBehavior(String selector, String method) {
		super(selector, method);
	}

	/**
	 * Instantiates a new Temporary j query behavior.
	 *
	 * @param selector the selector
	 */
	public TemporaryJQueryBehavior(String selector) {
		super(selector);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isTemporary(Component component) {
		return true;
	}
}
