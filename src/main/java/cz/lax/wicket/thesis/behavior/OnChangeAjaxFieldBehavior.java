package cz.lax.wicket.thesis.behavior;

import cz.lax.wicket.thesis.component.*;
import org.apache.wicket.ajax.*;
import org.apache.wicket.ajax.form.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class OnChangeAjaxFieldBehavior extends AjaxFormComponentUpdatingBehavior {
	private static final long serialVersionUID = 1L;

	private FormRowPanel formRowPanel;

	/**
	 * Construct.
	 *
	 * @param formRowPanel the form row panel
	 */
	public OnChangeAjaxFieldBehavior(FormRowPanel formRowPanel) {
		super("onchange");

		this.formRowPanel = formRowPanel;
	}

	/**
	 * On update.
	 *
	 * @param target the target
	 */
	@Override
	protected void onUpdate(AjaxRequestTarget target) {
		formRowPanel.onSuccess(target);
	}

	/**
	 * On error.
	 *
	 * @param target the target
	 * @param e the e
	 */
	@Override
	protected void onError(AjaxRequestTarget target, RuntimeException e) {
		formRowPanel.onError(target);
		super.onError(target, e);
	}
}
