package cz.lax.wicket.thesis.behavior.jquery;

import com.googlecode.wicket.jquery.core.*;
import org.apache.wicket.resource.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class SelectBoxItBehavior extends JQueryBehavior {
	private static final long serialVersionUID = 1L;

	private static final String METHOD = "selectBoxIt";

	// default select box it options
	public static final Options DEFAULT_OPTIONS = new Options()
			.set("showEffect", Options.asString("fadeIn"))
			.set("showEffectSpeed", 400)
			.set("hideEffect", Options.asString("fadeOut"))
			.set("hideEffectSpeed", 400)
			.set("showFirstOption", false);

	/**
	 * Instantiates a new Select box it behavior.
	 *
	 * @param selector the selector
	 */
	public SelectBoxItBehavior(String selector) {
		this(selector, DEFAULT_OPTIONS);
	}

	/**
	 * Instantiates a new Select box it behavior.
	 *
	 * @param selector the selector
	 * @param options the options
	 */
	public SelectBoxItBehavior(String selector, Options options) {
		super(selector, METHOD, options);

		add(new JQueryPluginResourceReference(SelectBoxItBehavior.class, "jquery.selectBoxIt.min.js"));
	}

}
