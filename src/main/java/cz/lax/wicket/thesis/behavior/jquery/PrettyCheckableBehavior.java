package cz.lax.wicket.thesis.behavior.jquery;

import com.googlecode.wicket.jquery.core.*;
import org.apache.wicket.resource.*;

/**
 * User: lukasjahoda
 * Name: Lukas Jahoda
 * Date: 08.03.14
 * Email: lukas@lukasjahoda.cz
 */
public class PrettyCheckableBehavior extends JQueryBehavior {
	private static final long serialVersionUID = 1L;

	public static final String METHOD = "prettyCheckable";

	/**
	 * Instantiates a new Pretty checkable behavior.
	 *
	 * @param selector the selector
	 */
	public PrettyCheckableBehavior(String selector) {
		this(selector, new Options());
	}

	/**
	 * Instantiates a new Pretty checkable behavior.
	 *
	 * @param selector the selector
	 * @param options the options
	 */
	public PrettyCheckableBehavior(String selector, Options options) {
		super(selector, METHOD, options);

		add(new JQueryPluginResourceReference(SelectBoxItBehavior.class, "prettyCheckable.min.js"));
	}

	/**
	 * We need to override the jQuery statement, because pretty checkable plugin needs to build for every input specificaly.
	 *
	 * @return jquery function
	 */
	@Override
	protected String $() {
		return String.format("jQuery(function() { jQuery('%s').each(function() { $(this).%s(%s); }); });", selector, method, options);
	}
}
