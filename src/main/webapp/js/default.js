if (typeof $ !== 'undefined') {
    (function($) {
        $.fn.formRowInit = function() {
            return this.each(function() {
                var me = $(this);
                me.find('p').on('click', function() {
                    me.removeClass('error');
                    me.find('input:visible').focus();
                });
                me.find('input:visible').on('focus input', function() {
                    me.removeClass('error');
                });

            });
        };
        $.fn.formRowOkError = function(options) {
            var settings = $.extend({
                'err' : false,
                'ok' : false
            }, options);
            return this.each(function() {
                var me = $(this);
                me.removeClass('error').removeClass('ok');
                if (settings.err) {
                    me.addClass('error');
                    if (me.hasClass('col')) {
                        me.find('label').hide();
                    }
                }
                if (settings.ok) {
                    me.addClass('ok');
                }
            });
        };
    })($);
}